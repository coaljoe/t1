__prototype__

props / info:

  - speed: 50 km/h
  - lenght: 160 m
  - range: sea ?
  - sensors:
    - radar
  - weapons:
    - 1x 130mm gun
    - 1x short-range surface-to-air SAM 24-rocket/cell ?
    - 1x 64 rocket vertical launch block (two blocks ?)
    - 1x 30mm rotary gun (defence ?)
  - features: ?
    - has_aircraft
      - aircraft_type: helicopter
    - has_landing_pad
    - has_hangar

extra:

  - [image link](https://ru.wikipedia.org/wiki/%D0%AD%D1%81%D0%BA%D0%B0%D0%B4%D1%80%D0%B5%D0%BD%D0%BD%D1%8B%D0%B5_%D0%BC%D0%B8%D0%BD%D0%BE%D0%BD%D0%BE%D1%81%D1%86%D1%8B_%D1%82%D0%B8%D0%BF%D0%B0_052D#/media/%D0%A4%D0%B0%D0%B9%D0%BB:%D0%A0%D0%B0%D0%B7%D0%BC%D0%B5%D1%89%D0%B5%D0%BD%D0%B8%D0%B5_%D0%B2%D0%BE%D0%BE%D1%80%D1%83%D0%B6%D0%B5%D0%BD%D0%B8%D1%8F_%D0%BD%D0%B0_%D1%8D%D1%81%D0%BC%D0%B8%D0%BD%D1%86%D0%B5_%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B0_52D.jpg)


weapons

?

- big rockets
- medium rockets
- small rockets