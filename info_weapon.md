?

props:

  - ? shot type: bursts / single_shot
  - ? reload time
    - used for reload ?

actual:

  - fire_delay : float

### Component ###

(json)

- fire_delay

(json)

| name              | type            | optional | is_type | info
| ---               | ---             | ---      | ---     | ---
| fire_delay        | float           |          |         | delay before the fire starts
| ammo_type         | string          | ?        |         | ?
| _ammo_type        | string (type)   | ?        | Y       | ?
| _ammo_types       | [string (type)] | ?        | Y ?     | accept ammo types / kinds ?
| ?_ammo_kinds      | [?]             | ?        |         |
| shot_delay        | float           | ?        |         | delay before the next shot starts
| first_shot_delay  | float           | Y ?      |         | delay before the first shot
| ammo_reload_delay | float           | Y ?      |         | ammo: (new) ammo reload delay

comments:

* ammo_reload_delay should be in ammo ?

questions:

* can load differnt ammo / ammo types ?
* can weapon be refit with different ammo ?
* how weapon know which ammo types it accepts ?
  * what if there were futher upgrade with now uknown ammo type / kind ?
  * what if new ammo kind / type is introduced ?
  * ammo / weapon compatibility ?
  
#### Examples ####

weapon1:

```
{
    "shot_speed": 10,
    "burst_delay" 10,
}
```

### Schema ###

?