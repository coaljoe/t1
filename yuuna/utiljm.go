package yuuna

import (
	"fmt"
	"os"
	_json "encoding/json"

	// XXX
	//"github.com/metalim/jsonmap"
	jsonmap "github.com/metalim/jsonmap/simplemap"

	"t1/yuuna/util/json"
)

func JmHasKey(jm *jsonmap.Map, key string) bool {
	_, ok := jm.Get(key)
	
	return ok
}

func JmGetMust(jm *jsonmap.Map, key string) interface{} {
	val, ok := jm.Get(key)
	if !ok {
		pp("jmGetMust error: key:", key)
	}
	
	return val
}

func JmGetAsMust[T any](jm *jsonmap.Map, key string) T {
	val, ok := jsonmap.GetAs[T](jm, key)
	if !ok {
		pp("jmGetMust error: key:", key)
	}
	
	return val
}

func JmConvArrayFloat(arri []interface{}) []float64 {
	ret := make([]float64, 0)

	for _, vi := range arri {
		v := vi.(float64)
		ret = append(ret, v)
	}
	
	return ret
}

// XXX not tested
/*
// XXX combine ordered maps ?
// XXX like in python dicts
func JmMerge(jm, jm2 *jsonmap.Map) *jsonmap.Map {
	res := jsonmap.New()
	
	// XXX copy left
	for _, k := range jm.Keys() {
		val, _ := jm.Get(k)
		res.Set(k, val)
	}

	// XXX copy-replace right	
	for _, k := range jm2.Keys() {
		val, _ := jm2.Get(k)
		//if !have {
		res.Set(k, val)
		//}
	}
	
	return res
}
*/

// constructor
func JsonmapFromJson(b []byte) *jsonmap.Map {

	xb := []byte(json.RemoveComments(string(b)))

	jm := jsonmap.New()

	//json.Unmarshal(b, &om)
	//err := om.UnmarshalJSON(b)
	err := jm.UnmarshalJSON(xb)

	if err != nil {
		panic(err)
	}

	return jm
}

//func jmDumpSimple(jm *jsonmap.Map) {
func JmDumpSimple(val interface{}) {
	
	//data, err := _json.Marshal(&jm)
	data, err := _json.Marshal(&val)
	if err != nil {
		panic(err)
	}
	//fmt.Println()
	fmt.Println("dump:")
	fmt.Println(string(data))
}

func JmDumpFile(val interface{}, path string) {

	f, err := os.Create(path)
	if err != nil {
		panic(err)
	}
	
	data, err := _json.Marshal(&val)
	if err != nil {
		panic(err)
	}

	f.Write(data)
	f.Close()
}
